-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 03-Jun-2019 às 21:33
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 7.3.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `estudocompleto-dev`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `bancas`
--

CREATE TABLE `bancas` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `bancas`
--

INSERT INTO `bancas` (`id`, `name`, `code`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Fundação Carlos Chagas', 'FCC', 'Fundação Carlos Chagas', '2019-05-03 02:08:42', NULL),
(2, 'Centro Brasileiro de Pesquisa em Avaliação e Seleção e de Promoção de', 'CESPE', 'Centro Brasileiro de Pesquisa em Avaliação e Seleção e de Promoção de Eventos - CEBRASPE', '2019-05-03 02:09:54', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cargos`
--

CREATE TABLE `cargos` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instituicao_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `cargos`
--

INSERT INTO `cargos` (`id`, `code`, `name`, `description`, `instituicao_id`, `created_at`, `updated_at`) VALUES
(3, 'AF-RFB', 'Auditor Fiscal', 'Auditor Fiscal', 6, '2019-05-09 21:36:07', NULL),
(4, 'A-INSS', 'Auditor INSS', 'Auditor INSS', 7, '2019-05-10 13:34:21', '2019-05-14 20:53:53');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cargos_disciplinas`
--

CREATE TABLE `cargos_disciplinas` (
  `id` int(10) UNSIGNED NOT NULL,
  `cargos_id` int(10) UNSIGNED NOT NULL,
  `disciplinas_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `daily_minutes` int(11) NOT NULL DEFAULT '0',
  `c_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `cargos_disciplinas`
--

INSERT INTO `cargos_disciplinas` (`id`, `cargos_id`, `disciplinas_id`, `created_at`, `updated_at`, `daily_minutes`, `c_order`) VALUES
(5, 3, 12, NULL, NULL, 0, 7),
(6, 3, 2, NULL, NULL, 0, 8),
(7, 3, 8, NULL, NULL, 0, 9),
(8, 3, 9, NULL, NULL, 0, 10),
(9, 3, 6, NULL, NULL, 0, 2),
(10, 3, 5, NULL, NULL, 0, 4),
(11, 3, 4, NULL, NULL, 0, 3),
(12, 3, 13, NULL, NULL, 0, 11),
(13, 3, 7, NULL, NULL, 0, 1),
(14, 3, 14, NULL, NULL, 0, 12),
(15, 3, 3, NULL, NULL, 0, 5),
(16, 3, 15, NULL, NULL, 0, 13),
(17, 3, 11, NULL, NULL, 0, 14),
(18, 3, 10, NULL, NULL, 0, 15),
(19, 3, 1, NULL, NULL, 0, 6),
(22, 4, 12, NULL, NULL, 0, 0),
(23, 4, 2, NULL, NULL, 0, 0),
(24, 4, 7, NULL, NULL, 0, 0),
(25, 4, 3, NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ciclos`
--

CREATE TABLE `ciclos` (
  `id` int(10) UNSIGNED NOT NULL,
  `week` int(11) NOT NULL,
  `date_class` datetime NOT NULL,
  `workload_prev` int(11) NOT NULL,
  `workload_real` varchar(11) COLLATE utf8mb4_unicode_ci NOT NULL,
  `class` int(11) NOT NULL,
  `pg_start` int(11) NOT NULL,
  `pg_end` int(11) NOT NULL,
  `concluded` tinyint(1) NOT NULL,
  `disciplina_id` int(10) UNSIGNED NOT NULL,
  `plano_estudo_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `revisao_id` int(10) UNSIGNED DEFAULT NULL,
  `ciclo_id_rev` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cidades`
--

CREATE TABLE `cidades` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `estado_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `cidades`
--

INSERT INTO `cidades` (`id`, `name`, `estado_id`, `created_at`, `updated_at`) VALUES
(1, 'Campinas', 26, '2019-05-03 02:03:09', NULL),
(2, 'São Paulo', 26, '2019-05-03 02:03:19', NULL),
(3, 'Brasília', 7, '2019-05-09 20:33:29', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_apicustom`
--

CREATE TABLE `cms_apicustom` (
  `id` int(10) UNSIGNED NOT NULL,
  `permalink` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_unicode_ci,
  `responses` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `cms_apicustom`
--

INSERT INTO `cms_apicustom` (`id`, `permalink`, `tabel`, `aksi`, `kolom`, `orderby`, `sub_query_1`, `sql_where`, `nama`, `keterangan`, `parameter`, `created_at`, `updated_at`, `method_type`, `parameters`, `responses`) VALUES
(2, 'cargos', 'cargos', 'list', NULL, NULL, NULL, NULL, 'cargos', NULL, NULL, NULL, NULL, 'get', 'a:0:{}', 'a:5:{i:0;a:4:{s:4:\"name\";s:2:\"id\";s:4:\"type\";s:3:\"int\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:1;a:4:{s:4:\"name\";s:4:\"code\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:2;a:4:{s:4:\"name\";s:4:\"name\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:3;a:4:{s:4:\"name\";s:11:\"description\";s:4:\"type\";s:6:\"string\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}i:4;a:4:{s:4:\"name\";s:14:\"instituicao_id\";s:4:\"type\";s:7:\"integer\";s:8:\"subquery\";N;s:4:\"used\";s:1:\"1\";}}');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_apikey`
--

CREATE TABLE `cms_apikey` (
  `id` int(10) UNSIGNED NOT NULL,
  `screetkey` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `cms_apikey`
--

INSERT INTO `cms_apikey` (`id`, `screetkey`, `hit`, `status`, `created_at`, `updated_at`) VALUES
(1, '9b9947db13a78bee5ae694cc21d7d0d5', 0, 'active', '2019-05-09 21:39:22', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_dashboard`
--

CREATE TABLE `cms_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_email_queues`
--

CREATE TABLE `cms_email_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci,
  `email_attachments` text COLLATE utf8mb4_unicode_ci,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_email_templates`
--

CREATE TABLE `cms_email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `cms_email_templates`
--

INSERT INTO `cms_email_templates` (`id`, `name`, `slug`, `subject`, `content`, `description`, `from_name`, `from_email`, `cc_email`, `created_at`, `updated_at`) VALUES
(1, 'Email Template Forgot Password Backend', 'forgot_password_backend', NULL, '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'system@crudbooster.com', NULL, '2019-05-02 19:12:10', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `cms_logs`
--

INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `details`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/login', 'admin@crudbooster.com login with IP Address 127.0.0.1', '', 1, '2019-05-02 22:39:54', NULL),
(2, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/logout', 'admin@crudbooster.com logout', '', 1, '2019-05-02 22:40:36', NULL),
(3, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/login', 'admin@crudbooster.com se logou com o endereo de IP 127.0.0.1', '', 1, '2019-05-02 23:36:20', NULL),
(4, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/logout', 'admin@crudbooster.com sair', '', 1, '2019-05-02 23:52:54', NULL),
(5, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/login', 'admin@crudbooster.com se logou com o endereo de IP 127.0.0.1', '', 1, '2019-05-03 01:40:26', NULL),
(6, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/module_generator/delete/12', 'Excluir o conteúdo de Estados em Module Generator', '', 1, '2019-05-03 01:46:03', NULL),
(7, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/cidades/add-save', 'Adicionar novo conteúdo de  em Cidades', '', 1, '2019-05-03 02:03:09', NULL),
(8, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/cidades/add-save', 'Adicionar novo conteúdo de  em Cidades', '', 1, '2019-05-03 02:03:19', NULL),
(9, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/instituicoes/add-save', 'Adicionar novo conteúdo de 15o Tribunal Regional do Trabalho em Instituições', '', 1, '2019-05-03 02:07:30', NULL),
(10, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/instituicoes/add-save', 'Adicionar novo conteúdo de Tribunal de Justiça de São Paulo em Instituições', '', 1, '2019-05-03 02:07:47', NULL),
(11, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/bancas/add-save', 'Adicionar novo conteúdo de Fundação Carlos Chagas em Bancas', '', 1, '2019-05-03 02:08:42', NULL),
(12, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/bancas/add-save', 'Adicionar novo conteúdo de Centro Brasileiro de Pesquisa em Avaliação e Seleção e de Promoção de em Bancas', '', 1, '2019-05-03 02:09:54', NULL),
(13, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/disciplinas/add-save', 'Adicionar novo conteúdo de Língua Portuguesa em Disciplinas', '', 1, '2019-05-03 02:14:11', NULL),
(14, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/disciplinas/add-save', 'Adicionar novo conteúdo de Administração Pública em Disciplinas', '', 1, '2019-05-03 02:14:45', NULL),
(15, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/disciplinas/add-save', 'Adicionar novo conteúdo de Raciocínio Lógico em Disciplinas', '', 1, '2019-05-03 02:15:27', NULL),
(16, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos/add-save', 'Adicionar novo conteúdo de Técnico e Analista Fiscal em Concursos', '', 1, '2019-05-03 02:21:20', NULL),
(17, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos/add-save', 'Adicionar novo conteúdo de Psicólogo Judiciário em Concursos', '', 1, '2019-05-03 02:23:13', NULL),
(18, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/add-save', 'Adicionar novo conteúdo de  em Concursos+Disciplinas', '', 1, '2019-05-03 02:35:31', NULL),
(19, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/add-save', 'Adicionar novo conteúdo de  em Concursos+Disciplinas', '', 1, '2019-05-03 02:35:46', NULL),
(20, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/add-save', 'Adicionar novo conteúdo de  em Concursos+Disciplinas', '', 1, '2019-05-03 02:36:02', NULL),
(21, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/edit-save/3', 'Atualizar dados de  em Concursos+Disciplinas', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-05-03 02:47:14', NULL),
(22, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/delete/3', 'Excluir o conteúdo de 3 em Concursos+Disciplinas', '', 1, '2019-05-03 02:47:42', NULL),
(23, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/edit-save/2', 'Atualizar dados de  em Concursos+Disciplinas', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-05-03 02:47:53', NULL),
(24, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/edit-save/2', 'Atualizar dados de  em Concursos+Disciplinas', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-05-03 02:48:15', NULL),
(25, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/add-save', 'Adicionar novo conteúdo de  em Concursos+Disciplinas', '', 1, '2019-05-03 02:48:38', NULL),
(26, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/login', 'admin@crudbooster.com se logou com o endereo de IP 127.0.0.1', '', 1, '2019-05-03 13:25:48', NULL),
(27, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/delete/4', 'Excluir o conteúdo de 4 em Concursos+Disciplinas', '', 1, '2019-05-03 13:27:31', NULL),
(28, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/delete/2', 'Excluir o conteúdo de 2 em Concursos+Disciplinas', '', 1, '2019-05-03 13:27:34', NULL),
(29, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/delete/1', 'Excluir o conteúdo de 1 em Concursos+Disciplinas', '', 1, '2019-05-03 13:27:37', NULL),
(30, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/add-save', 'Adicionar novo conteúdo de  em Concursos+Disciplinas', '', 1, '2019-05-03 13:27:58', NULL),
(31, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/add-save', 'Adicionar novo conteúdo de  em Concursos+Disciplinas', '', 1, '2019-05-03 13:30:12', NULL),
(32, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/add-save', 'Adicionar novo conteúdo de  em Concursos+Disciplinas', '', 1, '2019-05-03 13:30:26', NULL),
(33, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/menu_management/edit-save/2', 'Atualizar dados de Estados em Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-map-o</td></tr></tbody></table>', 1, '2019-05-03 13:31:52', NULL),
(34, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/menu_management/edit-save/3', 'Atualizar dados de Cidades em Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-map-marker</td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 1, '2019-05-03 13:32:43', NULL),
(35, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/menu_management/edit-save/4', 'Atualizar dados de Bancas em Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-check-circle</td></tr><tr><td>sorting</td><td>3</td><td></td></tr></tbody></table>', 1, '2019-05-03 13:33:26', NULL),
(36, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/menu_management/edit-save/6', 'Atualizar dados de Disciplinas em Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-book</td></tr><tr><td>sorting</td><td>5</td><td></td></tr></tbody></table>', 1, '2019-05-03 13:35:48', NULL),
(37, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/menu_management/edit-save/7', 'Atualizar dados de Concursos em Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-pencil-square-o</td></tr><tr><td>sorting</td><td>6</td><td></td></tr></tbody></table>', 1, '2019-05-03 13:36:31', NULL),
(38, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/menu_management/edit-save/7', 'Atualizar dados de Concursos em Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>icon</td><td>fa fa-pencil-square-o</td><td>fa fa-pencil</td></tr><tr><td>sorting</td><td>6</td><td></td></tr></tbody></table>', 1, '2019-05-03 13:36:55', NULL),
(39, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/add-save', 'Adicionar novo conteúdo de  em Concursos+Disciplinas', '', 1, '2019-05-03 13:45:20', NULL),
(40, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/add-save', 'Adicionar novo conteúdo de  em Concursos+Disciplinas', '', 1, '2019-05-03 13:45:34', NULL),
(41, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/add-save', 'Adicionar novo conteúdo de  em Concursos+Disciplinas', '', 1, '2019-05-03 13:45:50', NULL),
(42, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/login', 'admin@crudbooster.com se logou com o endereo de IP 127.0.0.1', '', 1, '2019-05-03 21:07:03', NULL),
(43, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/logout', 'admin@crudbooster.com sair', '', 1, '2019-05-03 21:07:36', NULL),
(44, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/login', 'admin@crudbooster.com se logou com o endereo de IP 127.0.0.1', '', 1, '2019-05-03 21:38:21', NULL),
(45, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/edit-save/6', 'Atualizar dados de  em Concursos+Disciplinas', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>total_pages</td><td>300</td><td>175</td></tr><tr><td>total_hours</td><td>40</td><td>8</td></tr></tbody></table>', 1, '2019-05-03 21:38:59', NULL),
(46, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/concursos_disciplinas/edit-save/10', 'Atualizar dados de  em Concursos+Disciplinas', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>total_hours</td><td>8</td><td>15</td></tr></tbody></table>', 1, '2019-05-03 21:39:09', NULL),
(47, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36', 'http://ec-2/admin/logout', 'admin@crudbooster.com sair', '', 1, '2019-05-03 21:39:21', NULL),
(48, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/login', 'admin@crudbooster.com se logou com o endereo de IP 127.0.0.1', '', 1, '2019-05-07 20:33:37', NULL),
(49, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/users/add-save', 'Adicionar novo conteúdo de Admin Estudo Completo em Users Management', '', 1, '2019-05-07 20:35:41', NULL),
(50, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/logout', 'admin@crudbooster.com sair', '', 1, '2019-05-07 20:39:09', NULL),
(51, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/login', 'admin@estudocompleto.com.br se logou com o endereo de IP 127.0.0.1', '', 2, '2019-05-08 14:44:42', NULL),
(52, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/logout', ' sair', '', NULL, '2019-05-09 20:03:01', NULL),
(53, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/login', 'admin@estudocompleto.com.br se logou com o endereo de IP 127.0.0.1', '', 2, '2019-05-09 20:03:12', NULL),
(54, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/module_generator/delete/19', 'Excluir o conteúdo de Concursos+Disciplinas em Module Generator', '', 2, '2019-05-09 20:10:34', NULL),
(55, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/module_generator/delete/20', 'Excluir o conteúdo de Cargos em Module Generator', '', 2, '2019-05-09 20:14:51', NULL),
(56, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/cargos/add-save', 'Adicionar novo conteúdo de Auditor/Fiscal em Cargos', '', 2, '2019-05-09 20:17:29', NULL),
(57, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/cidades/add-save', 'Adicionar novo conteúdo de  em Cidades', '', 2, '2019-05-09 20:33:29', NULL),
(58, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/instituicoes/add-save', 'Adicionar novo conteúdo de Receita Federal Brasil em Instituições', '', 2, '2019-05-09 20:34:23', NULL),
(59, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/cargos/edit-save/1', 'Atualizar dados de Auditor/Fiscal em Cargos', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>instituicao_id</td><td>1</td><td>6</td></tr></tbody></table>', 2, '2019-05-09 20:39:11', NULL),
(60, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/disciplinas/edit-save/3', 'Atualizar dados de Exatas em Disciplinas', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>code</td><td>RL</td><td>EX</td></tr><tr><td>name</td><td>Raciocínio Lógico</td><td>Exatas</td></tr><tr><td>description</td><td>Raciocínio Lógico</td><td>Matemática e Raciocínio lógico</td></tr></tbody></table>', 2, '2019-05-09 20:40:54', NULL),
(61, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/disciplinas/add-save', 'Adicionar novo conteúdo de Direito Constitucional em Disciplinas', '', 2, '2019-05-09 20:41:32', NULL),
(62, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/disciplinas/add-save', 'Adicionar novo conteúdo de Direito Administrativo em Disciplinas', '', 2, '2019-05-09 20:41:47', NULL),
(63, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/disciplinas/add-save', 'Adicionar novo conteúdo de Contabilidade geral em Disciplinas', '', 2, '2019-05-09 20:41:58', NULL),
(64, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/disciplinas/add-save', 'Adicionar novo conteúdo de Direito Tributário em Disciplinas', '', 2, '2019-05-09 20:42:14', NULL),
(65, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/disciplinas/add-save', 'Adicionar novo conteúdo de Auditoria em Disciplinas', '', 2, '2019-05-09 20:42:28', NULL),
(66, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/disciplinas/add-save', 'Adicionar novo conteúdo de Comércio Internacional em Disciplinas', '', 2, '2019-05-09 20:43:02', NULL),
(67, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/disciplinas/add-save', 'Adicionar novo conteúdo de Legislação Tributária Federal em Disciplinas', '', 2, '2019-05-09 20:43:25', NULL),
(68, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/disciplinas/add-save', 'Adicionar novo conteúdo de Legislação Aduaneira em Disciplinas', '', 2, '2019-05-09 20:43:47', NULL),
(69, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/disciplinas/add-save', 'Adicionar novo conteúdo de Administração Geral em Disciplinas', '', 2, '2019-05-09 20:44:04', NULL),
(70, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/disciplinas/add-save', 'Adicionar novo conteúdo de Direito Previdenciário em Disciplinas', '', 2, '2019-05-09 20:44:18', NULL),
(71, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/disciplinas/add-save', 'Adicionar novo conteúdo de Estatística em Disciplinas', '', 2, '2019-05-09 20:44:33', NULL),
(72, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/disciplinas/add-save', 'Adicionar novo conteúdo de Inglês/Espanhol em Disciplinas', '', 2, '2019-05-09 20:44:45', NULL),
(73, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/module_generator/delete/21', 'Excluir o conteúdo de Cargos em Module Generator', '', 2, '2019-05-09 21:14:24', NULL),
(74, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/cargos/add-save', 'Adicionar novo conteúdo de Fiscal INSS em Cargos', '', 2, '2019-05-09 21:30:09', NULL),
(75, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/cargos/delete/1', 'Excluir o conteúdo de Auditor/Fiscal em Cargos', '', 2, '2019-05-09 21:34:08', NULL),
(76, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/cargos/delete/2', 'Excluir o conteúdo de Fiscal INSS em Cargos', '', 2, '2019-05-09 21:34:11', NULL),
(77, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/cargos/delete/2', 'Excluir o conteúdo de Fiscal INSS em Cargos', '', 2, '2019-05-09 21:34:23', NULL),
(78, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/cargos/edit-save/2', 'Atualizar dados de Fiscal INSS em Cargos', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 2, '2019-05-09 21:34:40', NULL),
(79, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/cargos/delete/2', 'Excluir o conteúdo de Fiscal INSS em Cargos', '', 2, '2019-05-09 21:34:44', NULL),
(80, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/cargos/add-save', 'Adicionar novo conteúdo de Auditor Fiscal em Cargos', '', 2, '2019-05-09 21:36:07', NULL),
(81, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/menu_management/edit-save/11', 'Atualizar dados de Cargos em Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>color</td><td></td><td>normal</td></tr><tr><td>icon</td><td>fa fa-glass</td><td>fa fa-cogs</td></tr><tr><td>sorting</td><td>8</td><td></td></tr></tbody></table>', 2, '2019-05-09 21:43:03', NULL),
(82, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/menu_management/add-save', 'Adicionar novo conteúdo de Institutos em Menu Management', '', 2, '2019-05-09 21:46:05', NULL),
(83, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/menu_management/add-save', 'Adicionar novo conteúdo de Cargos em Menu Management', '', 2, '2019-05-09 21:48:35', NULL),
(84, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/menu_management/edit-save/12', 'Atualizar dados de Instituitos em Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>name</td><td>Institutos</td><td>Instituitos</td></tr><tr><td>sorting</td><td>2</td><td></td></tr></tbody></table>', 2, '2019-05-09 21:49:37', NULL),
(85, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/menu_management/add-save', 'Adicionar novo conteúdo de Concursos em Menu Management', '', 2, '2019-05-09 21:50:10', NULL),
(86, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/menu_management/edit-save/13', 'Atualizar dados de Cargos em Menu Management', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody><tr><td>icon</td><td></td><td>fa fa-cog</td></tr></tbody></table>', 2, '2019-05-09 21:51:05', NULL),
(87, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/instituicoes/add-save', 'Adicionar novo conteúdo de Instituto Nacional do Seguro Social em Instituições', '', 2, '2019-05-09 22:01:25', NULL),
(88, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/instituicoes/delete/1', 'Excluir o conteúdo de 15o Tribunal Regional do Trabalho em Instituições', '', 2, '2019-05-09 22:06:44', NULL),
(89, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/instituicoes/delete/1', 'Excluir o conteúdo de 15o Tribunal Regional do Trabalho em Instituições', '', 2, '2019-05-09 22:07:03', NULL),
(90, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/login', 'admin@estudocompleto.com.br se logou com o endereo de IP 127.0.0.1', '', 2, '2019-05-10 13:03:59', NULL),
(91, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/cargos/add-save', 'Adicionar novo conteúdo de Auditor INSS em Cargos', '', 2, '2019-05-10 13:34:21', NULL),
(92, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/login', 'admin@estudocompleto.com.br se logou com o endereo de IP 127.0.0.1', '', 2, '2019-05-13 22:37:25', NULL),
(93, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/login', 'admin@crudbooster.com se logou com o endereo de IP 127.0.0.1', '', 1, '2019-05-14 20:14:06', NULL),
(94, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/cargos/edit-save/4', 'Atualizar dados de Auditor INSS em Cargos', '<table class=\"table table-striped\"><thead><tr><th>Key</th><th>Old Value</th><th>New Value</th></thead><tbody></tbody></table>', 1, '2019-05-14 20:53:54', NULL),
(95, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36', 'http://estudocompleto-dev/admin/login', 'admin@estudocompleto.com.br se logou com o endereo de IP 127.0.0.1', '', 2, '2019-05-17 20:29:02', NULL),
(96, '127.0.0.1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36', 'http://estudocompleto-dev/admin/login', 'admin@estudocompleto.com.br se logou com o endereo de IP 127.0.0.1', '', 2, '2019-05-22 20:29:55', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `name`, `type`, `path`, `color`, `icon`, `parent_id`, `is_active`, `is_dashboard`, `id_cms_privileges`, `sorting`, `created_at`, `updated_at`) VALUES
(2, 'Estados', 'Route', 'AdminEstadosControllerGetIndex', 'normal', 'fa fa-map-o', 12, 1, 0, 1, 3, '2019-05-03 01:46:26', '2019-05-03 13:31:52'),
(3, 'Cidades', 'Route', 'AdminCidadesControllerGetIndex', 'normal', 'fa fa-map-marker', 12, 1, 0, 1, 2, '2019-05-03 01:54:29', '2019-05-03 13:32:43'),
(4, 'Bancas', 'Route', 'AdminBancasControllerGetIndex', 'normal', 'fa fa-check-circle', 14, 1, 0, 1, 2, '2019-05-03 02:03:41', '2019-05-03 13:33:26'),
(5, 'Instituições', 'Route', 'AdminInstituicoesControllerGetIndex', NULL, 'fa fa-building', 12, 1, 0, 1, 1, '2019-05-03 02:05:16', NULL),
(6, 'Disciplinas', 'Route', 'AdminDisciplinasControllerGetIndex', 'normal', 'fa fa-book', 13, 1, 0, 1, 2, '2019-05-03 02:11:57', '2019-05-03 13:35:48'),
(7, 'Concursos', 'Route', 'AdminConcursosControllerGetIndex', 'normal', 'fa fa-pencil', 14, 1, 0, 1, 1, '2019-05-03 02:16:11', '2019-05-03 13:36:55'),
(11, 'Cargos', 'Route', 'AdminCargosControllerGetIndex', 'normal', 'fa fa-cogs', 13, 1, 0, 1, 1, '2019-05-09 21:14:39', '2019-05-09 21:43:02'),
(12, 'Instituitos', 'Module', 'instituicoes', 'normal', 'fa fa-building-o', 0, 1, 0, 1, 2, '2019-05-09 21:46:05', '2019-05-09 21:49:37'),
(13, 'Cargos', 'Module', 'cargos', 'normal', 'fa fa-cog', 0, 1, 0, 1, 1, '2019-05-09 21:48:35', '2019-05-09 21:51:05'),
(14, 'Concursos', 'Module', 'concursos', 'normal', 'fa fa-pencil', 0, 1, 0, 1, 3, '2019-05-09 21:50:10', NULL),
(15, 'Cargos-Disciplinas', 'Route', 'AdminCargosDisciplinasControllerGetIndex', NULL, 'fa fa-glass', 13, 1, 0, 1, 3, '2019-05-14 20:14:55', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_menus_privileges`
--

CREATE TABLE `cms_menus_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_menus` int(11) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `cms_menus_privileges`
--

INSERT INTO `cms_menus_privileges` (`id`, `id_cms_menus`, `id_cms_privileges`) VALUES
(1, 1, 1),
(5, 5, 1),
(8, 8, 1),
(9, 2, 1),
(10, 3, 1),
(11, 4, 1),
(12, 6, 1),
(14, 7, 1),
(15, 9, 1),
(16, 10, 1),
(18, 11, 1),
(21, 12, 1),
(22, 14, 1),
(23, 13, 1),
(24, 15, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_moduls`
--

CREATE TABLE `cms_moduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2019-05-02 19:12:10', NULL, NULL),
(2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2019-05-02 19:12:10', NULL, NULL),
(3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2019-05-02 19:12:10', NULL, NULL),
(4, 'Users Management', 'fa fa-users', 'users', 'cms_users', 'AdminCmsUsersController', 0, 1, '2019-05-02 19:12:10', NULL, NULL),
(5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2019-05-02 19:12:10', NULL, NULL),
(6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2019-05-02 19:12:10', NULL, NULL),
(7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2019-05-02 19:12:10', NULL, NULL),
(8, 'Email Templates', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2019-05-02 19:12:10', NULL, NULL),
(9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2019-05-02 19:12:10', NULL, NULL),
(10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2019-05-02 19:12:10', NULL, NULL),
(11, 'Log User Access', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2019-05-02 19:12:10', NULL, NULL),
(12, 'Estados', 'fa fa-glass', 'estados', 'estados', 'AdminEstadosController', 0, 0, '2019-05-03 01:40:57', NULL, '2019-05-03 01:46:03'),
(13, 'Estados', 'fa fa-glass', 'estados', 'estados', 'AdminEstadosController', 0, 0, '2019-05-03 01:46:26', NULL, NULL),
(14, 'Cidades', 'fa fa-glass', 'cidades', 'cidades', 'AdminCidadesController', 0, 0, '2019-05-03 01:54:28', NULL, NULL),
(15, 'Bancas', 'fa fa-glass', 'bancas', 'bancas', 'AdminBancasController', 0, 0, '2019-05-03 02:03:41', NULL, NULL),
(16, 'Instituições', 'fa fa-building-o', 'instituicoes', 'instituicoes', 'AdminInstituicoesController', 0, 0, '2019-05-03 02:05:16', NULL, NULL),
(17, 'Disciplinas', 'fa fa-building', 'disciplinas', 'disciplinas', 'AdminDisciplinasController', 0, 0, '2019-05-03 02:11:56', NULL, NULL),
(18, 'Concursos', 'fa fa-glass', 'concursos', 'concursos', 'AdminConcursosController', 0, 0, '2019-05-03 02:16:10', NULL, NULL),
(19, 'Concursos+Disciplinas', 'fa fa-pencil-square-o', 'concursos_disciplinas', 'concursos_disciplinas', 'AdminConcursosDisciplinasController', 0, 0, '2019-05-03 02:24:10', NULL, '2019-05-09 20:10:34'),
(20, 'Cargos', 'fa fa-glass', 'cargos', 'cargos', 'AdminCargosController', 0, 0, '2019-05-09 20:13:11', NULL, '2019-05-09 20:14:51'),
(21, 'Cargos', 'fa fa-glass', 'cargos', 'cargos', 'AdminCargosController', 0, 0, '2019-05-09 20:15:04', NULL, '2019-05-09 21:14:24'),
(22, 'Cargos', 'fa fa-glass', 'cargos', 'cargos', 'AdminCargosController', 0, 0, '2019-05-09 21:14:39', NULL, NULL),
(23, 'Cargos-Disciplinas', 'fa fa-glass', 'cargos_disciplinas', 'cargos_disciplinas', 'AdminCargosDisciplinasController', 0, 0, '2019-05-14 20:14:55', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_notifications`
--

CREATE TABLE `cms_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_privileges`
--

CREATE TABLE `cms_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `cms_privileges`
--

INSERT INTO `cms_privileges` (`id`, `name`, `is_superadmin`, `theme_color`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrator', 1, 'skin-red', '2019-05-02 19:12:10', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_privileges_roles`
--

CREATE TABLE `cms_privileges_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `cms_privileges_roles`
--

INSERT INTO `cms_privileges_roles` (`id`, `is_visible`, `is_create`, `is_read`, `is_edit`, `is_delete`, `id_cms_privileges`, `id_cms_moduls`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, 0, 0, 1, 1, '2019-05-02 19:12:10', NULL),
(2, 1, 1, 1, 1, 1, 1, 2, '2019-05-02 19:12:10', NULL),
(3, 0, 1, 1, 1, 1, 1, 3, '2019-05-02 19:12:10', NULL),
(4, 1, 1, 1, 1, 1, 1, 4, '2019-05-02 19:12:10', NULL),
(5, 1, 1, 1, 1, 1, 1, 5, '2019-05-02 19:12:10', NULL),
(6, 1, 1, 1, 1, 1, 1, 6, '2019-05-02 19:12:10', NULL),
(7, 1, 1, 1, 1, 1, 1, 7, '2019-05-02 19:12:10', NULL),
(8, 1, 1, 1, 1, 1, 1, 8, '2019-05-02 19:12:10', NULL),
(9, 1, 1, 1, 1, 1, 1, 9, '2019-05-02 19:12:10', NULL),
(10, 1, 1, 1, 1, 1, 1, 10, '2019-05-02 19:12:10', NULL),
(11, 1, 0, 1, 0, 1, 1, 11, '2019-05-02 19:12:10', NULL),
(12, 1, 1, 1, 1, 1, 1, 12, NULL, NULL),
(13, 1, 1, 1, 1, 1, 1, 13, NULL, NULL),
(14, 1, 1, 1, 1, 1, 1, 14, NULL, NULL),
(15, 1, 1, 1, 1, 1, 1, 15, NULL, NULL),
(16, 1, 1, 1, 1, 1, 1, 16, NULL, NULL),
(17, 1, 1, 1, 1, 1, 1, 17, NULL, NULL),
(18, 1, 1, 1, 1, 1, 1, 18, NULL, NULL),
(19, 1, 1, 1, 1, 1, 1, 19, NULL, NULL),
(20, 1, 1, 1, 1, 1, 1, 20, NULL, NULL),
(21, 1, 1, 1, 1, 1, 1, 21, NULL, NULL),
(22, 1, 1, 1, 1, 1, 1, 22, NULL, NULL),
(23, 1, 1, 1, 1, 1, 1, 23, NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_settings`
--

CREATE TABLE `cms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_input_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `cms_settings`
--

INSERT INTO `cms_settings` (`id`, `name`, `content`, `content_input_type`, `dataenum`, `helper`, `created_at`, `updated_at`, `group_setting`, `label`) VALUES
(1, 'login_background_color', NULL, 'text', NULL, 'Input hexacode', '2019-05-02 19:12:10', NULL, 'Login Register Style', 'Login Background Color'),
(2, 'login_font_color', NULL, 'text', NULL, 'Input hexacode', '2019-05-02 19:12:10', NULL, 'Login Register Style', 'Login Font Color'),
(3, 'login_background_image', NULL, 'upload_image', NULL, NULL, '2019-05-02 19:12:10', NULL, 'Login Register Style', 'Login Background Image'),
(4, 'email_sender', 'support@crudbooster.com', 'text', NULL, NULL, '2019-05-02 19:12:10', NULL, 'Email Setting', 'Email Sender'),
(5, 'smtp_driver', 'mail', 'select', 'smtp,mail,sendmail', NULL, '2019-05-02 19:12:10', NULL, 'Email Setting', 'Mail Driver'),
(6, 'smtp_host', '', 'text', NULL, NULL, '2019-05-02 19:12:10', NULL, 'Email Setting', 'SMTP Host'),
(7, 'smtp_port', '25', 'text', NULL, 'default 25', '2019-05-02 19:12:10', NULL, 'Email Setting', 'SMTP Port'),
(8, 'smtp_username', '', 'text', NULL, NULL, '2019-05-02 19:12:10', NULL, 'Email Setting', 'SMTP Username'),
(9, 'smtp_password', '', 'text', NULL, NULL, '2019-05-02 19:12:10', NULL, 'Email Setting', 'SMTP Password'),
(10, 'appname', 'Estudo Completo', 'text', NULL, NULL, '2019-05-02 19:12:10', NULL, 'Application Setting', 'Application Name'),
(11, 'default_paper_size', 'A4', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2019-05-02 19:12:10', NULL, 'Application Setting', 'Default Paper Print Size'),
(12, 'logo', 'uploads/2019-05/0b2e4a90fbba17252457b0800eb8c4d1.jpeg', 'upload_image', NULL, NULL, '2019-05-02 19:12:10', NULL, 'Application Setting', 'Logo'),
(13, 'favicon', 'uploads/2019-05/490c9a22e51430546ed09d908164632b.png', 'upload_image', NULL, NULL, '2019-05-02 19:12:10', NULL, 'Application Setting', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2019-05-02 19:12:10', NULL, 'Application Setting', 'API Debug Mode'),
(15, 'google_api_key', NULL, 'text', NULL, NULL, '2019-05-02 19:12:10', NULL, 'Application Setting', 'Google API Key'),
(16, 'google_fcm_key', NULL, 'text', NULL, NULL, '2019-05-02 19:12:10', NULL, 'Application Setting', 'Google FCM Key');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_statistics`
--

CREATE TABLE `cms_statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_statistic_components`
--

CREATE TABLE `cms_statistic_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `cms_users`
--

INSERT INTO `cms_users` (`id`, `name`, `photo`, `email`, `password`, `id_cms_privileges`, `created_at`, `updated_at`, `status`) VALUES
(1, 'Super Admin', NULL, 'admin@crudbooster.com', '$2y$10$/nSAXhAQnwEGg/2T./N/pu0xsM.zzCsPvbIY2n5SoYQw//6LayXDW', 1, '2019-05-02 19:12:10', NULL, 'Active'),
(2, 'Admin Estudo Completo', 'uploads/1/2019-05/img_460145.png', 'admin@estudocompleto.com.br', '$2y$10$JI7I.AkGctSBRxo.2vOUM.QFyBTmPxro4bOBvEuDlsFqZNwIRo9GG', 1, '2019-05-07 20:35:41', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `concursos`
--

CREATE TABLE `concursos` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exam_date` datetime NOT NULL,
  `instituicao_id` int(10) UNSIGNED NOT NULL,
  `banca_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `concursos`
--

INSERT INTO `concursos` (`id`, `code`, `name`, `description`, `exam_date`, `instituicao_id`, `banca_id`, `created_at`, `updated_at`) VALUES
(1, '01/2019', 'Técnico e Analista Fiscal', 'Técnico e Analista Fiscal', '2019-06-30 00:00:00', 1, 1, '2019-05-03 02:21:20', NULL),
(2, '12/2019', 'Psicólogo Judiciário', 'Psicólogo Judiciário', '2019-06-02 00:00:00', 2, 2, '2019-05-03 02:23:13', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `disciplinas`
--

CREATE TABLE `disciplinas` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `disciplinas`
--

INSERT INTO `disciplinas` (`id`, `code`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'LP', 'Língua Portuguesa', 'Língua Portuguesa', '2019-05-03 02:14:11', NULL),
(2, 'AP', 'Administração Pública', 'Administração Pública', '2019-05-03 02:14:45', NULL),
(3, 'EX', 'Exatas', 'Matemática e Raciocínio lógico', '2019-05-03 02:15:27', '2019-05-09 20:40:54'),
(4, 'DC', 'Direito Constitucional', 'Direito Constitucional', '2019-05-09 20:41:32', NULL),
(5, 'DA', 'Direito Administrativo', 'Direito Administrativo', '2019-05-09 20:41:47', NULL),
(6, 'CG', 'Contabilidade geral', 'Contabilidade geral', '2019-05-09 20:41:58', NULL),
(7, 'DT', 'Direito Tributário', 'Direito Tributário', '2019-05-09 20:42:14', NULL),
(8, 'AU', 'Auditoria', 'Auditoria', '2019-05-09 20:42:28', NULL),
(9, 'CI', 'Comércio Internacional', 'Comércio Internacional', '2019-05-09 20:43:02', NULL),
(10, 'LT', 'Legislação Tributária Federal', 'Legislação Tributária Federal', '2019-05-09 20:43:25', NULL),
(11, 'LA', 'Legislação Aduaneira', 'Legislação Aduaneira', '2019-05-09 20:43:47', NULL),
(12, 'AG', 'Administração Geral', 'Administração Geral', '2019-05-09 20:44:04', NULL),
(13, 'DP', 'Direito Previdenciário', 'Direito Previdenciário', '2019-05-09 20:44:18', NULL),
(14, 'ES', 'Estatística', 'Estatística', '2019-05-09 20:44:33', NULL),
(15, 'IE', 'Inglês/Espanhol', 'Inglês/Espanhol', '2019-05-09 20:44:45', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `estados`
--

CREATE TABLE `estados` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `estados`
--

INSERT INTO `estados` (`id`, `name`, `code`, `created_at`, `updated_at`) VALUES
(1, 'Acre', 'AC', NULL, NULL),
(2, 'Alagoas', 'AL', NULL, NULL),
(3, 'Amazonas', 'AM', NULL, NULL),
(4, 'Amapá', 'AP', NULL, NULL),
(5, 'Bahia', 'BA', NULL, NULL),
(6, 'Ceará', 'CE', NULL, NULL),
(7, 'Distrito Federal', 'DF', NULL, NULL),
(8, 'Espírito Santo', 'ES', NULL, NULL),
(9, 'Goiás', 'GO', NULL, NULL),
(10, 'Maranhão', 'MA', NULL, NULL),
(11, 'Minas Gerais', 'MG', NULL, NULL),
(12, 'Mato Grosso do Sul', 'MS', NULL, NULL),
(13, 'Mato Grosso', 'MT', NULL, NULL),
(14, 'Pará', 'PA', NULL, NULL),
(15, 'Paraíba', 'PB', NULL, NULL),
(16, 'Pernambuco', 'PE', NULL, NULL),
(17, 'Piauí', 'PI', NULL, NULL),
(18, 'Paraná', 'PR', NULL, NULL),
(19, 'Rio de Janeiro', 'RJ', NULL, NULL),
(20, 'Rio Grande do Norte', 'RN', NULL, NULL),
(21, 'Rondônia', 'RO', NULL, NULL),
(22, 'Roraima', 'RR', NULL, NULL),
(23, 'Rio Grande do Sul', 'RS', NULL, NULL),
(24, 'Santa Catarina', 'SC', NULL, NULL),
(25, 'Sergipe', 'SE', NULL, NULL),
(26, 'São Paulo', 'SP', NULL, NULL),
(27, 'Tocantins', 'TO', NULL, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `instituicoes`
--

CREATE TABLE `instituicoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cidade_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `instituicoes`
--

INSERT INTO `instituicoes` (`id`, `name`, `code`, `cidade_id`, `created_at`, `updated_at`) VALUES
(1, '15o Tribunal Regional do Trabalho', 'TRT15', 1, '2019-05-03 02:07:30', NULL),
(2, 'Tribunal de Justiça de São Paulo', 'TJSP', 2, '2019-05-03 02:07:47', NULL),
(6, 'Receita Federal Brasil', 'RFB', 3, '2019-05-09 20:34:23', NULL),
(7, 'Instituto Nacional do Seguro Social', 'INSS', 3, '2019-05-09 22:01:25', NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `materiais`
--

CREATE TABLE `materiais` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `q_classes` int(11) NOT NULL,
  `disciplina_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_08_07_145904_add_table_cms_apicustom', 2),
(4, '2016_08_07_150834_add_table_cms_dashboard', 2),
(5, '2016_08_07_151210_add_table_cms_logs', 2),
(6, '2016_08_07_151211_add_details_cms_logs', 2),
(7, '2016_08_07_152014_add_table_cms_privileges', 2),
(8, '2016_08_07_152214_add_table_cms_privileges_roles', 2),
(9, '2016_08_07_152320_add_table_cms_settings', 2),
(10, '2016_08_07_152421_add_table_cms_users', 2),
(11, '2016_08_07_154624_add_table_cms_menus_privileges', 2),
(12, '2016_08_07_154624_add_table_cms_moduls', 2),
(13, '2016_08_17_225409_add_status_cms_users', 2),
(14, '2016_08_20_125418_add_table_cms_notifications', 2),
(15, '2016_09_04_033706_add_table_cms_email_queues', 2),
(16, '2016_09_16_035347_add_group_setting', 2),
(17, '2016_09_16_045425_add_label_setting', 2),
(18, '2016_09_17_104728_create_nullable_cms_apicustom', 2),
(19, '2016_10_01_141740_add_method_type_apicustom', 2),
(20, '2016_10_01_141846_add_parameters_apicustom', 2),
(21, '2016_10_01_141934_add_responses_apicustom', 2),
(22, '2016_10_01_144826_add_table_apikey', 2),
(23, '2016_11_14_141657_create_cms_menus', 2),
(24, '2016_11_15_132350_create_cms_email_templates', 2),
(25, '2016_11_15_190410_create_cms_statistics', 2),
(26, '2016_11_17_102740_create_cms_statistic_components', 2),
(27, '2017_06_06_164501_add_deleted_at_cms_moduls', 2),
(28, '2019_05_02_161826_create_estados_table', 3),
(29, '2019_05_02_161912_create_cidades_table', 3),
(30, '2019_05_02_161937_create_bancas_table', 3),
(31, '2019_05_02_162039_create_instituicoes_table', 3),
(32, '2019_05_02_162113_create_concursos_table', 3),
(33, '2019_05_02_162138_create_disciplinas_table', 3),
(34, '2019_05_02_162220_create_plano__estudos_table', 3),
(35, '2019_05_02_162334_create_observacoes_table', 3),
(36, '2019_05_02_162358_create_questoes_table', 3),
(37, '2019_05_02_162622_create_concurso_disciplinas_table', 4),
(38, '2019_05_02_192330_add_cpf_and_phone_to_users_table', 4),
(39, '2019_05_02_194230_add_cpf_and_phone_to_users_table', 5),
(40, '2019_05_02_232549_add_totals_to_concursos_disciplinas_table', 6),
(41, '2019_05_08_190717_drop_concursos_disciplinas_table', 7),
(42, '2019_05_08_191255_create_cargos_table', 8),
(43, '2019_05_08_191621_create_materiais_table', 9),
(44, '2019_05_09_124332_drop_columns_on_plano_estudos_table', 10),
(45, '2019_05_09_133835_add_columns_on_plano_estudos_table', 11),
(46, '2019_05_09_135039_create_ciclos_table', 12),
(47, '2019_05_09_142111_alter_foreign_questoes_table', 13),
(48, '2019_05_09_142801_alter_foreign_observacoes_table', 14),
(49, '2019_05_09_165430_create_revisoes_table', 15),
(50, '2019_05_09_180127_create_cargos_disciplinas_table', 16),
(51, '2019_05_10_095326_add_daily_work_plano_estudos_table', 17),
(52, '2019_05_14_133359_add_daily_hour_order_to_cargos_disciplinas_table', 18),
(53, '2019_05_22_173800_alter_foreign_revisoes_table', 19),
(54, '2019_05_22_174923_add_foreign_ciclo_table', 20),
(55, '2019_05_23_141909_add_field_revisoes_table', 20),
(56, '2019_05_23_142923_add_field_ciclos_table', 21),
(57, '2019_05_30_163114_alter_foreign_questoes2_table', 22),
(58, '2019_06_03_085826_alter_foreign_questoes-3_table', 23);

-- --------------------------------------------------------

--
-- Estrutura da tabela `observacoes`
--

CREATE TABLE `observacoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ciclo_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `plano_estudos`
--

CREATE TABLE `plano_estudos` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `cargo_id` int(10) UNSIGNED NOT NULL,
  `material_id` int(10) UNSIGNED DEFAULT NULL,
  `daily_work` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `questoes`
--

CREATE TABLE `questoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `class` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `q_completed` int(11) NOT NULL,
  `q_correct` int(11) NOT NULL,
  `topic` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `ciclo_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `revisoes`
--

CREATE TABLE `revisoes` (
  `id` int(10) UNSIGNED NOT NULL,
  `rev_days` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `plano_estudos_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cpf` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `cpf`, `phone`) VALUES
(1, 'Anderson Cassoli', 'andersoncassoli@gmail.com', '$2y$10$FfyDX7Fq7uzYZEaFtMwNsu7gsLmjYGUV6mG8SEauKRWM8COeILLh.', 'KZn5rd6BuXiPfDYsUxsxiOhd2a3Aqo01rDqIVfVTpi1DBliR7NPxONQaspVH', '2019-05-02 22:39:23', '2019-05-02 22:39:23', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bancas`
--
ALTER TABLE `bancas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cargos`
--
ALTER TABLE `cargos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cargos_instituicao_id_foreign` (`instituicao_id`);

--
-- Indexes for table `cargos_disciplinas`
--
ALTER TABLE `cargos_disciplinas`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cargos_disciplinas_cargo_id_foreign` (`cargos_id`),
  ADD KEY `cargos_disciplinas_disciplina_id_foreign` (`disciplinas_id`);

--
-- Indexes for table `ciclos`
--
ALTER TABLE `ciclos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ciclos_disciplina_id_foreign` (`disciplina_id`),
  ADD KEY `ciclos_plano_estudo_id_foreign` (`plano_estudo_id`),
  ADD KEY `ciclos_ciclo_id_rev_foreign` (`ciclo_id_rev`);

--
-- Indexes for table `cidades`
--
ALTER TABLE `cidades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cidades_estado_id_foreign` (`estado_id`);

--
-- Indexes for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_logs`
--
ALTER TABLE `cms_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus`
--
ALTER TABLE `cms_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_settings`
--
ALTER TABLE `cms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `concursos`
--
ALTER TABLE `concursos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `concursos_instituicao_id_foreign` (`instituicao_id`),
  ADD KEY `concursos_banca_id_foreign` (`banca_id`);

--
-- Indexes for table `disciplinas`
--
ALTER TABLE `disciplinas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `estados`
--
ALTER TABLE `estados`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instituicoes`
--
ALTER TABLE `instituicoes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `instituicoes_cidade_id_foreign` (`cidade_id`);

--
-- Indexes for table `materiais`
--
ALTER TABLE `materiais`
  ADD PRIMARY KEY (`id`),
  ADD KEY `materiais_disciplina_id_foreign` (`disciplina_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `observacoes`
--
ALTER TABLE `observacoes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `observacoes_ciclo_id_foreign` (`ciclo_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `plano_estudos`
--
ALTER TABLE `plano_estudos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `plano_estudos_user_id_foreign` (`user_id`),
  ADD KEY `plano_estudos_cargo_id_foreign` (`cargo_id`),
  ADD KEY `plano_estudos_material_id_foreign` (`material_id`);

--
-- Indexes for table `questoes`
--
ALTER TABLE `questoes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questoes_ciclo_id_foreign` (`ciclo_id`);

--
-- Indexes for table `revisoes`
--
ALTER TABLE `revisoes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `revisoes_plano_estudos_id_foreign` (`plano_estudos_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bancas`
--
ALTER TABLE `bancas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cargos`
--
ALTER TABLE `cargos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `cargos_disciplinas`
--
ALTER TABLE `cargos_disciplinas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `ciclos`
--
ALTER TABLE `ciclos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=175;

--
-- AUTO_INCREMENT for table `cidades`
--
ALTER TABLE `cidades`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_logs`
--
ALTER TABLE `cms_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `cms_menus`
--
ALTER TABLE `cms_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `cms_menus_privileges`
--
ALTER TABLE `cms_menus_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `cms_settings`
--
ALTER TABLE `cms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `concursos`
--
ALTER TABLE `concursos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `disciplinas`
--
ALTER TABLE `disciplinas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `estados`
--
ALTER TABLE `estados`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `instituicoes`
--
ALTER TABLE `instituicoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `materiais`
--
ALTER TABLE `materiais`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `observacoes`
--
ALTER TABLE `observacoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `plano_estudos`
--
ALTER TABLE `plano_estudos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `questoes`
--
ALTER TABLE `questoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `revisoes`
--
ALTER TABLE `revisoes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `cargos`
--
ALTER TABLE `cargos`
  ADD CONSTRAINT `cargos_instituicao_id_foreign` FOREIGN KEY (`instituicao_id`) REFERENCES `instituicoes` (`id`);

--
-- Limitadores para a tabela `cargos_disciplinas`
--
ALTER TABLE `cargos_disciplinas`
  ADD CONSTRAINT `cargos_disciplinas_cargo_id_foreign` FOREIGN KEY (`cargos_id`) REFERENCES `cargos` (`id`),
  ADD CONSTRAINT `cargos_disciplinas_disciplina_id_foreign` FOREIGN KEY (`disciplinas_id`) REFERENCES `disciplinas` (`id`);

--
-- Limitadores para a tabela `ciclos`
--
ALTER TABLE `ciclos`
  ADD CONSTRAINT `ciclos_ciclo_id_rev_foreign` FOREIGN KEY (`ciclo_id_rev`) REFERENCES `ciclos` (`id`),
  ADD CONSTRAINT `ciclos_disciplina_id_foreign` FOREIGN KEY (`disciplina_id`) REFERENCES `disciplinas` (`id`),
  ADD CONSTRAINT `ciclos_plano_estudo_id_foreign` FOREIGN KEY (`plano_estudo_id`) REFERENCES `plano_estudos` (`id`);

--
-- Limitadores para a tabela `cidades`
--
ALTER TABLE `cidades`
  ADD CONSTRAINT `cidades_estado_id_foreign` FOREIGN KEY (`estado_id`) REFERENCES `estados` (`id`);

--
-- Limitadores para a tabela `concursos`
--
ALTER TABLE `concursos`
  ADD CONSTRAINT `concursos_banca_id_foreign` FOREIGN KEY (`banca_id`) REFERENCES `bancas` (`id`),
  ADD CONSTRAINT `concursos_instituicao_id_foreign` FOREIGN KEY (`instituicao_id`) REFERENCES `instituicoes` (`id`);

--
-- Limitadores para a tabela `instituicoes`
--
ALTER TABLE `instituicoes`
  ADD CONSTRAINT `instituicoes_cidade_id_foreign` FOREIGN KEY (`cidade_id`) REFERENCES `cidades` (`id`);

--
-- Limitadores para a tabela `materiais`
--
ALTER TABLE `materiais`
  ADD CONSTRAINT `materiais_disciplina_id_foreign` FOREIGN KEY (`disciplina_id`) REFERENCES `disciplinas` (`id`);

--
-- Limitadores para a tabela `observacoes`
--
ALTER TABLE `observacoes`
  ADD CONSTRAINT `observacoes_ciclo_id_foreign` FOREIGN KEY (`ciclo_id`) REFERENCES `ciclos` (`id`);

--
-- Limitadores para a tabela `plano_estudos`
--
ALTER TABLE `plano_estudos`
  ADD CONSTRAINT `plano_estudos_cargo_id_foreign` FOREIGN KEY (`cargo_id`) REFERENCES `cargos` (`id`),
  ADD CONSTRAINT `plano_estudos_material_id_foreign` FOREIGN KEY (`material_id`) REFERENCES `materiais` (`id`),
  ADD CONSTRAINT `plano_estudos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Limitadores para a tabela `questoes`
--
ALTER TABLE `questoes`
  ADD CONSTRAINT `questoes_ciclo_id_foreign` FOREIGN KEY (`ciclo_id`) REFERENCES `ciclos` (`id`);

--
-- Limitadores para a tabela `revisoes`
--
ALTER TABLE `revisoes`
  ADD CONSTRAINT `revisoes_plano_estudos_id_foreign` FOREIGN KEY (`plano_estudos_id`) REFERENCES `plano_estudos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
