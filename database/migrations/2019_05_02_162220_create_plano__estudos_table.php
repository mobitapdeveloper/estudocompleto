<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlanoEstudosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plano_estudos', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date_class');
            $table->integer('workload_prev');
            $table->integer('workload_real');
            $table->integer('class');
            $table->integer('pg_start');
            $table->integer('pg_end');
            $table->boolean('concluded');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('concurso_id');
            $table->unsignedInteger('disciplina_id');
            $table->timestamps();
            
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('concurso_id')->references('id')->on('concursos');
            $table->foreign('disciplina_id')->references('id')->on('disciplinas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plano__estudos');
    }
}
