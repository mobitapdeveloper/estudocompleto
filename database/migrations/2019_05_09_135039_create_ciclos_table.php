<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCiclosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ciclos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('week');
            $table->datetime('date_class');
            $table->integer('workload_prev');
            $table->integer('workload_real');
            $table->integer('class');
            $table->integer('pg_start');
            $table->integer('pg_end');
            $table->boolean('concluded');
            $table->unsignedInteger('disciplina_id');
            $table->unsignedInteger('plano_estudo_id');
            $table->timestamps();

            $table->foreign('disciplina_id')->references('id')->on('disciplinas');
            $table->foreign('plano_estudo_id')->references('id')->on('plano_estudos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ciclos');
    }
}
