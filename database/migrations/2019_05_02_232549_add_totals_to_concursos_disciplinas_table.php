<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalsToConcursosDisciplinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('concursos_disciplinas', function($table) {
            $table->integer('total_pages');
            $table->integer('total_hours');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('concursos_disciplinas', function($table) {
            $table->dropColumn('total_pages');
            $table->dropColumn('total_hours');
        });
    }
}
