<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterForeignRevisoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('revisoes', function (Blueprint $table) {
            $table->dropForeign('revisoes_ciclo_id_foreign');
            $table->dropColumn('ciclo_id');
            $table->unsignedInteger('plano_estudos_id');
            $table->foreign('plano_estudos_id')->references('id')->on('plano_estudos');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
