<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConcursoDisciplinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('concursos_disciplinas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('concurso_id');
            $table->unsignedInteger('disciplina_id');
            $table->timestamps();

            $table->foreign('concurso_id')->references('id')->on('concursos');
            $table->foreign('disciplina_id')->references('id')->on('disciplinas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
