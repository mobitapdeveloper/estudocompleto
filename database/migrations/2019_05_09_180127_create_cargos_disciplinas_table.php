<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCargosDisciplinasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('cargos_disciplinas', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('cargo_id');
                $table->unsignedInteger('disciplina_id');
                $table->timestamps();
    
                $table->foreign('cargo_id')->references('id')->on('cargos');
                $table->foreign('disciplina_id')->references('id')->on('disciplinas');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cargos_disciplinas');
    }
}
