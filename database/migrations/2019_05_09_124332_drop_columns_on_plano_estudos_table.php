<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnsOnPlanoEstudosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plano_estudos', function (Blueprint $table) {
            $table->dropColumn('date_class');
            $table->dropColumn('workload_prev');
            $table->dropColumn('workload_real');
            $table->dropColumn('class');
            $table->dropColumn('pg_start');
            $table->dropColumn('pg_end');
            $table->dropColumn('concluded');
            $table->dropForeign('plano_estudos_concurso_id_foreign');
            $table->dropColumn('concurso_id');
            $table->dropForeign('plano_estudos_disciplina_id_foreign');
            $table->dropColumn('disciplina_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
