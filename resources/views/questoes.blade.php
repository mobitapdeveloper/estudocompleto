<!DOCTYPE html>
<html>
<head>

    <title>Ciclo de Estudos - Estudo Completo</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>

</head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">Questões</div>
                            <div class="panel-body">
                                @if (session('status'))
                                    <div class="alert alert-success">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                {!! Form::open(['route' => ['question_store', $ciclo->id], 'class' => 'form']) !!}
                                    <div class="form-row">
                                        <div class="form-group col-md-4">
                                            <label for="inputName4">Aula</label>
                                            {!! Form::text('class', null, ['class' => 'form-control', 'placeholder' => 'Aula']) !!}
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="inputName4">Feitas</label>
                                            {!! Form::text('q_completed', null, ['class' => 'form-control', 'placeholder' => 'Quantidade feita']) !!}
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="inputName4">Acertos</label>
                                            {!! Form::text('q_correct', null, ['class' => 'form-control', 'placeholder' => 'Quantidade acertos']) !!}
                                        </div>
                                        <div class="form-group col-md-4">
                                            <label for="inputName4">Assunto</label>
                                            {!! Form::text('topic', null, ['class' => 'form-control', 'placeholder' => 'Assunto']) !!}
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="col-md-12">
                                            <label for="inputName"> </label>
                                            {!! Form::submit('Enviar', ['class' => 'btn btn-primary']) !!}
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </body>
</html>