@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Bem vindo ao Estudo Completo</div>
                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        {!! Form::open(['route' => 'ciclo.store', 'class' => 'form']) !!}
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="inputName">Selecione o concurso desejado</label>
                                    {!! Form::select('cargo', $cargos, null, ['class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-12">
                                    <label for="inputName"> </label>
                                    {!! Form::submit('Criar Ciclo de Estudo', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
