@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Plano de Estudo: {{$concurso->name}}</div>
                    <div class="panel-body">
                        <table class="table table-bordered table-striped">
                            <tr>
                                <th>Data</th>
                                <th>Disciplina</th>
                                <th>Carga H. Prev.</th>
                                <th>Carg H. Efetiva</th>
                                <th>Aula</th>
                                <th>Pg. Inicio</th>
                                <th>Pg. Final</th>
                                <th>Terminou?</th>
                                <th></th>
                            </tr>
                            @foreach($plano_estudo as $p)
                            <tr>
                                <td>{{$p->DataEstudo}}</td>
                                <td>{{$p->Disciplinas}}</td>
                                <td>{{$p->CH_Prevista}}</td>
                                <td>{{$p->CH_Real}}</td>
                                <td>{{$p->Aula}}</td>
                                <td>{{$p->Pg_Inicial}}</td>
                                <td>{{$p->Pg_Final}}</td>
                                @if( $p->Concluida == "1" )
                                    <td><a href="" class="action activeup"><i class="glyphicon glyphicon-thumbs-up"></i></i></a></td>
                                @else
                                    <td><a href="" class="action activedown"><i class="glyphicon glyphicon-thumbs-down"></i></i></a></td>
                                @endif
                                <td><a href="" class="btn btn-outline-primary btn-sm" role="button" aria-pressed="true"><i class="glyphicon glyphicon-pencil"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
