<!DOCTYPE html>
<html>
<head>

    <title>Ciclo de Estudos - Estudo Completo</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css" rel="stylesheet"/>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
    <script src="https://kit.fontawesome.com/61ca1f1e85.js"></script>

</head>
    <body>
        <div class="container">
            <div>
                <span style="float:left;">
                    <h3>Ciclo de Estudos</h3>
                </span>
                <span style="float:right; padding-top:20px;">
                    {!! Form::open(['route' => ['create_new_cicle', $p_id], 'class' => 'form']) !!}
                        {!! Form::submit('Criar Novo Ciclo', ['class' => 'btn btn-primary']) !!}
                    {!! Form::close() !!}
                </span>
            </div>
        </div>
            <div class="container"></div>
            <div id="exTab2" class="container">	
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a  href="#1" data-toggle="tab">Hoje</a>
                    </li>
                    <li>
                        <a href="#2" data-toggle="tab">Semana</a>
                    </li>
                </ul>
                <div class="tab-content ">
                    <div class="tab-pane active" id="1">
                    <table class="table table-bordered">
                            <tr>
                                <td>{{ $today }}</td>
                            </tr>
                            <tr>
                                <th>Disciplina</th>
                                <th>Previsão de Horas</th>
                                <th>Horas Estudadas</th>
                                <th>Aula</th>
                                <th>Pg. Inicial</th>
                                <th>Pg. Final</th>
                                <th>Concluida</th>
                                <th></th>

                            </tr>
                            @foreach($ciclod as $dciclo)
                                <tr>
                                    <td>{{ $dciclo->disciplina }}</td>                        
                                    <td>{{ $dciclo->workload_prev }}</td>
                                    <td><a href="" class="update" data-name="workload_real" data-type="text" data-pk="{{ $dciclo->id }}" data-title="">{{ $dciclo->workload_real }}</a></td>
                                    <td><a href="" class="update" data-name="class" data-type="text" data-pk="{{ $dciclo->id }}" data-title="">{{ $dciclo->class }}</a></td>
                                    <td><a href="" class="update" data-name="pg_start" data-type="text" data-pk="{{ $dciclo->id }}" data-title="">{{ $dciclo->pg_start }}</a></td>
                                    <td><a href="" class="update" data-name="pg_end" data-type="text" data-pk="{{ $dciclo->id }}" data-title="">{{ $dciclo->pg_end }}</a></td>
                                    <td><a href="" class="options" data-name="concluded" data-type="select" data-pk="{{ $dciclo->id }}" data-title=""></a></td>
                                    <td>
                                        <a href="/question/{{ $dciclo->id }}" onclick="window.open('/question/{{ $dciclo->id }}', 'newwindow', 'width=600,height=420'); return false;">
                                            <i class="fas fa-question-circle"></i>
                                        </a> 
                                        <a href="/comment/{{ $dciclo->id }}" onclick="window.open('/comment/{{ $dciclo->id }}', 'newwindow', 'width=600,height=420'); return false;">
                                            <i class="fas fa-comment-dots"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach        
                        </table>
                    </div>
                    <div class="tab-pane" id="2">
                        <table class="table table-bordered">
                            <tr>
                                <th>Dia</th>
                                <th>Disciplina</th>                                
                                <th>Previsão de Horas</th>
                                <th>Horas Estudadas</th>
                                <th>Aula</th>
                                <th>Pg. Inicial</th>
                                <th>Pg. Final</th>
                                <th>Concluida</th>
                            </tr>
                            @php
                                $rowid = 0;
                                $rowspan = 0;
                            @endphp
                            @foreach($ciclos as $key => $ciclo)
                                @php
                                    $rowid += 1
                                @endphp
                                <tr>
                                    @if ($key == 0 || $rowspan == $rowid)
                                        @php
                                            $rowid = 0;
                                            $rowspan = $ciclo->reg_day;
                                        @endphp
                                        <td rowspan="{{ $rowspan }}" style="vertical-align : middle;text-align:center;">{{ $ciclo->weekday }} - {{ $ciclo->day_class }}</td>
                                    @endif
                                    
                                    <td>{{ $ciclo->disciplina }}</td>                        
                                    <td>{{ $ciclo->workload_prev }}</td>
                                    <td><a href="" class="update" data-name="workload_real" data-type="text" data-pk="{{ $ciclo->id }}" data-title="">{{ $ciclo->workload_real }}</a></td>
                                    <td><a href="" class="update" data-name="class" data-type="text" data-pk="{{ $ciclo->id }}" data-title="">{{ $ciclo->class }}</a></td>
                                    <td><a href="" class="update" data-name="pg_start" data-type="text" data-pk="{{ $ciclo->id }}" data-title="">{{ $ciclo->pg_start }}</a></td>
                                    <td><a href="" class="update" data-name="pg_end" data-type="text" data-pk="{{ $ciclo->id }}" data-title="">{{ $ciclo->pg_end }}</a></td>
                                    <td><a href="" class="options" data-name="concluded" data-type="select" data-pk="{{ $ciclo->id }}" data-title=""></a></td>
                                </tr>
                            @endforeach  
                        </table>
                    </div>
                </div>
            </div>
    </body>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.fn.editable.defaults.mode = 'inline';
        $('.update').editable({
            url: '/update_ciclo',
            type: 'text',
            pk: 1,
            name: 'name',
            title: 'Enter name',
            showbuttons: false,
        });
        $('.options').editable({
            url: '/update_ciclo',
            pk: 1,
            name: 'name',
            showbuttons: false,
            value: [0, 1],    
            source: [
                {value: 0, text: 'Não'},
                {value: 1, text: 'Sim'},
            ]
        });

    </script>
</html>