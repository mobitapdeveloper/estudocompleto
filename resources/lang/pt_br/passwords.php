<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Senha precisa ter pelo menos 6 caracteres e corresponder aos critérios.',
    'reset' => 'Sua senha está sendo redefinida!',
    'sent' => 'Nós estamos enviando um link para redefinição da senha!',
    'token' => 'Senha para redefinição inválida.',
    'user' => "Nós não encontramos o e-mail correspondente para redefinição.",

];
