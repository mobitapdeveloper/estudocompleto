<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Estados extends Model
{
    public function cidades(){
        return $this->hasMany('App\Models\Cidades', 'id', 'estado_id');
    }
}
