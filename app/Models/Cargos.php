<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cargos extends Model
{
    public function instituicoes(){
        return $this->belongsTo('App\Instituicoes', 'id', 'instituicao_id');
    } 

    public function disciplinas(){
        return $this->belongsToMany('App\Models\Disciplinas', 'cargos_disciplinas', 'cargo_id', 'disciplina_id');
    }
}
