<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Disciplinas extends Model
{
    public function cargos(){
        return $this->belongsToMany('App\Models\Cargos', 'cargos_disciplinas', 'disciplina_id', 'cargo_id');
    }

    public function ciclos(){   
         return $this->hasMany('App\Ciclos', 'ciclos', 'id', 'disciplina_id');
        }
}
