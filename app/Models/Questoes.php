<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Questoes extends Model
{
    protected $fillable = [
        'class', 'q_completed', 'q_correct', 'topic', 'ciclo_id',
    ];

    public function ciclos(){
        return $this-belongsTo('App\Models\Ciclos', 'ciclo_id', 'id');
    }
}
