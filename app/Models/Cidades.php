<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cidades extends Model
{
    public function estado(){
        return $this->belongsTo('App\Models\Estados', 'estado_id', 'id');
    }
}
