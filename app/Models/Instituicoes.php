<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Instituicoes extends Model
{
    public function cidade(){
        return $this->belongsTo('App\Models\Cidades', 'cidade_id', 'id');
    }

    public function banca(){
        return $this-belongsTo('App\Models\Bancas', 'banca_id', 'id');
    }
}
