<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Plano_Estudos extends Model
{
    public function user(){
        return $this->belongsTo('App\Users', 'id', 'user_id');
    } 

    public function concurso(){
        return $this->belongsTo('App\Models\Concursos', 'id', 'concurso_id');
    } 

    public function disciplina(){
        return $this->belongsTo('App\Models\Disciplinas', 'id', 'disciplina_id');
    } 
}
