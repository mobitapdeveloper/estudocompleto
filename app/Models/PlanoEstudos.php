<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlanoEstudos extends Model
{
    public function users(){
        return $this->belongsTo('App\Users', 'id', 'user_id');
    } 

    public function cargos(){
        return $this->belongsTo('App\Models\Cargos', 'id', 'cargo_id');
    } 

    public function materiais(){
        return $this->belongsTo('App\Models\Materiais', 'id', 'material_id');
    } 

    public function ciclos(){
        return $this->hasMany('App\Ciclos', 'ciclos', 'id', 'disciplina_id');
    }
}
