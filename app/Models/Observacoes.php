<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Observacoes extends Model
{
    protected $fillable = [
        'comment',  'ciclo_id',
    ];
    
    public function ciclos(){
        return $this-belongsTo('App\Models\Ciclos', 'ciclo_id', 'id');
    }
}
