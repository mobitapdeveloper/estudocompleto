<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ciclos extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'workload_real', 'class', 'pg_start', 'pg_end', 'concluded',
    ];

    public function plano_estudos(){
        return $this->belongsTo('App\Models\PlanoEstudos', 'plano_estudo_id', 'id');
    }

    public function disciplinas(){
        return $this-belongsTo('App\Models\Disciplinas', 'disciplina_id', 'id');
    }

    public function revisoes(){
        return $this-belongsTo('App\Models\Revisoes', 'revisao_id', 'id');
    }

}
