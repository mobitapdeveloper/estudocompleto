<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Concursos extends Model
{
    public function disciplinas(){
        return $this->belongsToMany('App\Models\Disciplinas', 'concursos_disciplinas', 'concurso_id', 'disciplina_id');
    }

    public function banca(){
        return $this->belongsTo('App\Models\Bancas', 'banca_id', 'id');
    }

    public function instituicao(){
        return $this-belongsTo('App\Models\Instituicoes', 'instituicao_id', 'id');
    }
}
