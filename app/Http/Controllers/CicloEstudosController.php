<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;
use App\Models\Estados;
use App\Models\Cidades;
use App\Models\Instituicoes;
use App\Models\Bancas;
use App\Models\Disciplinas;
use App\Models\Concursos;
use App\Models\PlanoEstudos;
use App\Models\Ciclos;
use App\Models\Revisoes;
use App\Http\Requests\PlanoEstudoRequest;


class CicloEstudosController extends Controller
{

    public function __construct(Disciplinas $disciplina, PlanoEstudos $plano, Ciclos $ciclo)
    {
        $this->middleware('auth');
        $this->disciplina = $disciplina;
        $this->plano = $plano;
        $this->ciclo - $ciclo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Verifica usuário, carrega da de hoje e obtém o ciclo semanal
        $user_id = auth()->user()->id;
        $today = Carbon::today()->toDateString();
        $ciclos = DB::table('ciclos')
        ->join('disciplinas', 'disciplinas.id', '=', 'disciplina_id')
        ->join('plano_estudos', 'plano_estudos.id', '=', 'plano_estudo_id')
        ->join('users', 'users.id', '=', 'plano_estudos.user_id')
        ->leftJoin('revisoes', 'ciclos.revisao_id', '=', 'revisoes.id')
        ->select('ciclos.id', 'ciclos.week', 'ciclos.date_class', 'ciclos.workload_prev', 'ciclos.workload_real', 'ciclos.class', 'ciclos.pg_start', 
        'ciclos.pg_end', 'ciclos.concluded',
        DB::raw('DATE_FORMAT(ciclos.date_class, "%d-%m-%Y") as day_class'),
        DB::raw('CASE WHEN ciclos.revisao_id IS NOT NULL THEN CONCAT(revisoes.name," - ",disciplinas.name) ELSE disciplinas.name END as disciplina'), 
        DB::raw('DAYNAME(ciclos.date_class) AS weekday'),
        DB::raw('(SELECT COUNT(id) FROM ciclos c WHERE c.date_class = ciclos.date_class GROUP BY c.date_class) as reg_day'))
        ->where('users.id', '=', $user_id)
        ->orderBy('ciclos.date_class', 'asc')
        ->orderBy('revisao_id', 'asc')
        ->orderBy('ciclo_id_rev', 'asc')
        ->get();
        
        // Obtém ciclo com os registros do dia.
        $ciclod = DB::table('ciclos')
        ->join('disciplinas', 'disciplinas.id', '=', 'disciplina_id')
        ->join('plano_estudos', 'plano_estudos.id', '=', 'plano_estudo_id')
        ->join('users', 'users.id', '=', 'plano_estudos.user_id')
        ->leftJoin('revisoes', 'ciclos.revisao_id', '=', 'revisoes.id')
        ->select('ciclos.id', 'ciclos.week', 'ciclos.date_class', 'ciclos.workload_prev', 'ciclos.workload_real', 'ciclos.class', 'ciclos.pg_start', 
        'ciclos.pg_end', 'ciclos.concluded', 
        DB::raw('CASE WHEN ciclos.revisao_id IS NOT NULL THEN CONCAT(revisoes.name," - ",disciplinas.name) ELSE disciplinas.name END as disciplina'), 
        DB::raw('DAYNAME(ciclos.date_class) AS weekday'))
        ->where('users.id', '=', $user_id)
        ->where('ciclos.date_class', '=', $today)
        ->orderBy('ciclos.id')
        ->get();

        $p_id = $this->plano->first()->id;
              
        return view('ciclos', compact('ciclos', 'ciclod', 'today', 'p_id'));        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // Recebe requisição (cargo)
        $dataForm = $request->all();
        $cargo_id = $dataForm['cargo'];

        // Insere plano com carga horária fixa (40)
        $insert = DB::table('plano_estudos')->insert([
            ['daily_work' => 40,
             'cargo_id' => $cargo_id,
             'user_id' => auth()->user()->id,
            ]
        ]);

        // Obtém último ID inserido
        $plano_estudo_id = DB::getPdo()->lastInsertId();

        // Insere revisão padrão 1 (24 horas)
        $insert = DB::table('revisoes')->insert([
            ['name' => 'Revisão 24 horas',
            'rev_days' => 1,
            'plano_estudos_id' => $plano_estudo_id,
            ]
        ]);

        // Insere revisão padrão 1 (7 dias)
        $insert = DB::table('revisoes')->insert([
            ['name' => 'Revisão 7 dias',
            'rev_days' => 7,
            'plano_estudos_id' => $plano_estudo_id,
            ]
        ]);

        // Insere 3 primeiras matérias e em seguida as próximas 3. Essa rotina deve ser substituída por rotina de geração automática no futuro.
        $disc_day_1 = DB::table('plano_estudos')
            ->join('cargos', 'plano_estudos.cargo_id', '=', 'cargos.id')
            ->join('cargos_disciplinas', 'cargos.id', '=', 'cargos_id')
            ->join('disciplinas', 'cargos_disciplinas.disciplinas_id', '=', 'disciplinas.id')
            ->select('plano_estudos.id as Plano_Estudos_ID', 'cargos.id as Cargo_ID', 'cargos.name as Cargo_Nome', 'disciplinas.id as Disciplina_ID', 
                'disciplinas.name as Disciplina_Nome', 'cargos_disciplinas.c_order as Ordem', 'plano_estudos.daily_work AS Horas_Diarias')
            ->where('plano_estudos.id', '=', $plano_estudo_id)->where('cargos_disciplinas.c_order', '<=', 3)
            ->orderBy('cargos_disciplinas.c_order')
            ->get();
        
        $disc_day_2 = DB::table('plano_estudos')
            ->join('cargos', 'plano_estudos.cargo_id', '=', 'cargos.id')
            ->join('cargos_disciplinas', 'cargos.id', '=', 'cargos_id')
            ->join('disciplinas', 'cargos_disciplinas.disciplinas_id', '=', 'disciplinas.id')
            ->select('plano_estudos.id as Plano_Estudos_ID', 'cargos.id as Cargo_ID', 'cargos.name as Cargo_Nome', 'disciplinas.id as Disciplina_ID', 
            'disciplinas.name as Disciplina_Nome', 'cargos_disciplinas.c_order as Ordem', 'plano_estudos.daily_work AS Horas_Diarias')
            ->where('plano_estudos.id', '=', $plano_estudo_id)->where('cargos_disciplinas.c_order', '>=', 4)->where('cargos_disciplinas.c_order', '<=', 6)
            ->orderBy('cargos_disciplinas.c_order')
            ->get();

        // Define datas inicial e final do ciclo
        $now = new Carbon();
        $dt_ini = new Carbon($this->now);
        $dt_fim = new Carbon($this->now);
        $dt_ini->startOfWeek()->toDateString();
        $dt_fim->endOfWeek()->toDateString();

        $day_of_week = $dt_ini;
        $inserts = [];

        // Circula pelos 6 dias da semana e insere as disciplinas em cada dia
        for($i = 1; $i <= 6; $i++)
        {
            if ($i % 2) {
                $query = $disc_day_2;
            } else {
                $query = $disc_day_1;
            } 
            foreach($query as $q) {
                $insert = DB::table('ciclos')->insert([
                    ['week' => 1,
                    'date_class' => $day_of_week,
                    'workload_prev' => $q->Horas_Diarias,
                    'workload_real' => 0,
                    'class' => 1,
                    'pg_start' => 1,
                    'pg_end' => 9999,
                    'concluded' => '0',
                    'plano_estudo_id' => $q->Plano_Estudos_ID,
                    'disciplina_id' => $q->Disciplina_ID,
                    ]
                ]);
                }
            $ciclo_id = DB::getPdo()->lastInsertId();
            $date_c = Ciclos::find($ciclo_id)->date_class;

            // Insere as revisões
            $revf = $this->createRevision($plano_estudo_id, $date_c);
            $day_of_week = $day_of_week->addDay();
        }
        return redirect()->route('ciclo.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateCiclo(Request $request)
    {
        // Usada para edição no grid (X-Editable) https://vitalets.github.io/x-editable/
        Ciclos::find($request->pk)->update([$request->name => $request->value]);

        // Atualiza revisões com os mesmos valores preenchidos nas disciplinas
        Ciclos::where('ciclo_id_rev', '=', $request->pk)->update([$request->name => $request->value]);
        return response()->json(['success'=>'done']);
    }

    public function createRevision($p_id, $date_c)
    {
        // Obtém as revisões cadastradas
        $revs = Revisoes::all();
        
        // Circula pelas revisões
        foreach ($revs as $revisao) {
            $dt = Carbon::parse($date_c);
            if ($dt->dayOfWeek == 1 && $revisao->rev_days = 1) {
                $dt->subDay($revisao->rev_days+1);
            } else {
                $dt->subDay($revisao->rev_days);
            }
            
            // Obtém registros do ciclo no dia recebido no parâmetro date_c
            $rev = DB::table('ciclos')
            ->join('disciplinas', 'ciclos.disciplina_id', '=', 'disciplinas.id')
            ->join('plano_estudos', 'ciclos.plano_estudo_id', '=', 'plano_estudos.id')
            ->select('ciclos.id as id', 'ciclos.week as week', 'ciclos.workload_prev as workload_prev',
            'ciclos.workload_real as workload_real',  'ciclos.class', 'ciclos.pg_start', 'ciclos.pg_end', 'ciclos.disciplina_id')
            ->where('ciclos.plano_estudo_id', '=', $p_id)
            ->where('date_class', '=', $dt)
            ->whereNull('ciclos.revisao_id')
            ->orderBy('ciclos.id')
            ->get(); 
            
            // Circula por cada registro do ciclo inserindo as revisões para o dia
            foreach($rev as $r) {
                $workl_p = intdiv($r->workload_prev, 4);
                $workl_r = intdiv($r->workload_real,4);
                $insert = DB::table('ciclos')->insert([
                    ['week' => $r->week,
                    'date_class' => $date_c,
                    'workload_prev' => $workl_p,
                    'workload_real' => $workl_r,
                    'class' => $r->class,
                    'pg_start' => $r->pg_start,
                    'pg_end' => $r->pg_end,
                    'concluded' => '0',
                    'plano_estudo_id' => $p_id,
                    'disciplina_id' => $r->disciplina_id,
                    'revisao_id' => $revisao->id,
                    'ciclo_id_rev' => $r->id,
                    ]
                ]);
            }
        }
    }

    public function createNewCicle($p_id)
    {
        // Obtém último ID do último ciclo
        $last_reg = DB::table('ciclos')
                ->where('plano_estudo_id', '=', $p_id)
                ->orderBy('id', 'desc')
                ->first();
        
        // Calcula primeiro dia do último ciclo
        $dt_end = $last_reg->date_class;
        $dt_start = new Carbon($dt_end);
        $dt_start->subDay(5);

        // Obtém os registros do último ciclo
        $last_cicle_days = DB::table('ciclos')
        ->select('ciclos.date_class')
        ->whereBetween('ciclos.date_class', [$dt_start, $dt_end] )
        ->groupBy('ciclos.date_class')
        ->get();

        // Calcula próximo número do contador de semanas
        $n_week = $last_reg->week + 1;

        // Circula pelos dias do último ciclo
        foreach ($last_cicle_days as $n) {
            // Calcula data dentro do novo ciclo
            $n_date = new Carbon($n->date_class);
            $n_date->addDay(7);

            // Obtém registros do último ciclo
            $last_cicle = DB::table('ciclos')
            ->whereNull('revisao_id')
            ->where('date_class', '=', $n->date_class)
            ->orderBy('id')
            ->get();

            // Circula pelos registros do último ciclo inserindo os valores no novo ciclo
            foreach ($last_cicle as $c) {
                $insert = DB::table('ciclos')->insert([
                    ['week' => $n_week,
                    'date_class' => $n_date,
                    'workload_prev' => $c->workload_prev,
                    'workload_real' => 0,
                    'class' => $c->class,
                    'pg_start' => $c->pg_end,
                    'pg_end' => 999999,
                    'concluded' => '0',
                    'plano_estudo_id' => $p_id,
                    'disciplina_id' => $c->disciplina_id,
                    ]
                ]);
            }

            // Insere revisões
            $revf = $this->createRevision($p_id, $n_date);
        }

        return redirect()->route('ciclo.index');
    }
    
}
