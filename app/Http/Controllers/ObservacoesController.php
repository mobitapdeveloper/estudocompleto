<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Ciclos;
use App\Models\Observacoes;

class ObservacoesController extends Controller
{
    public function create($id)
    {
        // Encontra registro no ciclo e abre página para entrada de valores
        $ciclo = Ciclos::find($id);
        return view('observacoes', compact('ciclo'));
    }

    public function store(Request $request, $c_id)
    {
        // Recebe o valor da observação e insere no banco
        $dataform = $request->all();
        $insert = DB::table('observacoes')->insert([
            ['comment' => $dataform['comment'],
            'ciclo_id' => $c_id,
            ]
        ]);
        if( $insert )
            return ('Observação atualizada com sucesso!');
        else
            return redirect()->route('comment_store', $c_id)->with(['errors' => 'Falha ao inserir...']);
    }
}