<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Ciclos;
use App\Models\Questoes;

class QuestoesController extends Controller
{
    public function create($id)
    {
        // Encontra ciclo e abre página para inserção das questões
        $ciclo = Ciclos::find($id);
        return view('questoes', compact('ciclo'));
    }

    public function store(Request $request, $c_id)
    {
        // Recebe valores e ID do ciclo
        $dataform = $request->all();

        // Insere valores no banco
        $insert = DB::table('questoes')->insert([
            ['class' => $dataform['class'],
            'q_completed' => $dataform['q_completed'],
            'q_correct' => $dataform['q_correct'],
            'topic' => $dataform['topic'],
            'ciclo_id' => $c_id,
            ]
        ]);
        if( $insert )
            return ('Questões atualizadas com sucesso!');
        else
            return redirect()->route('question_store', $c_id)->with(['errors' => 'Falha ao inserir...']);
    }
}
