<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Auth;
use App\Models\Estados;
use App\Models\Cidades;
use App\Models\Instituicoes;
use App\Models\Bancas;
use App\Models\Disciplinas;
use App\Models\Cargos;
use App\Models\PlanoEstudos;
use App\Models\Ciclos;
use App\Http\Requests\PlanoEstudoRequest;

class HomeController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Cargos $cargo, PlanoEstudos $plano)
    {
        $this->middleware('auth');
        $this->cargo = $cargo;
        $this->plano = $plano;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Obtem os cargos cadastrados
        $cargs = $this->cargo->all();
        foreach ($cargs as $cargo) {
            $cargos[$cargo->id] = $cargo->name;
        }
        
        // Verifica se usuário já tem plano de estudo e ciclo criado
        $plano = $this->plano->where('user_id', auth()->user()->id)->first();

        // Redireciona para página de criação do novo plano (home) ou para o ciclo.
        if( $plano )
            return redirect()->route('ciclo.index');
        else
            return view('home')->withCargos($cargos);        
    }

    public function teste()
    {
        // Função usada apenas para teste
        /*
        $str = file_get_contents('http://estudocompleto-dev/api/cargos');
        $json = json_decode($str, true);
        //$cargos = $json->data()->get();
        //$cargos = Cargos::pluck('name', 'id');
        $day = Carbon::now()->formatLocalized('%B');
        dd($day);

        $now = new Carbon();
        $dt = new Carbon($this->now);
        $dt->startOfWeek()->format('Y-m-d H:i');
        $dt->endOfWeek()->addDay()->toDateString();
        return $dt;

        $dt = Carbon::parse(now());

        dd($dt->dayOfWeek);
        //$estados = Estados::all();
        //dd($estados);
        */
        return view('teste', compact('estados'));


        //$ciclos = Ciclos::find(8457);
        //dd($ciclos);
    }

}
