<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/cria_ciclo_estudo', 'HomeController@teste');
Route::get('/planos_estudo/{id}', 'HomeController@planos_estudo')->name('planos_estudo');
Route::get('/teste', 'HomeController@teste')->name('teste');
Route::post('/update_ciclo','CicloEstudosController@updateCiclo');
Route::post('/create_new_cicle/{id}','CicloEstudosController@createNewCicle')->name('create_new_cicle');
Route::get('/question/{id}', 'QuestoesController@create')->name('question');
Route::post('/question_store/{id}', 'QuestoesController@store')->name('question_store');
Route::get('/comment/{id}', 'ObservacoesController@create')->name('comment');
Route::post('/comment_store/{id}', 'ObservacoesController@store')->name('comment_store');

Route::resource('/ciclo', 'CicloEstudosController');